"""
OS-specific API.
"""

import platform


if platform.system() == 'Windows':
    from .iolibwin import *     # NOQA

